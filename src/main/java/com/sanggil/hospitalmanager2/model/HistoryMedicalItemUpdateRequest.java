package com.sanggil.hospitalmanager2.model;

import com.sanggil.hospitalmanager2.enums.MedicalItem;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class HistoryMedicalItemUpdateRequest {
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private MedicalItem medicalItem;
}
