package com.sanggil.hospitalmanager2.model;

import com.sanggil.hospitalmanager2.entity.ClinicHistory;
import com.sanggil.hospitalmanager2.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class HistoryItem {
    private Long historyId;

    private Long customerId;

    private String customerName;

    private String customerPhone;

    private String registrationNumber;

    private String medicalItemName;

    private Double price;

    private String isSalaryName;

    private LocalDate dateCure;

    private LocalTime timeCure;

    private String isCalculate;

    private HistoryItem(HistoryItemBuilder builder) {
        this.historyId = builder.historyId;
        this.customerId = builder.customerId;
        this.customerName = builder.customerName;
        this.customerPhone = builder.customerPhone;
        this.registrationNumber = builder.registrationNumber;
        this.medicalItemName = builder.medicalItemName;
        this.price = builder.price;
        this.isSalaryName = builder.isSalaryName;
        this.dateCure = builder.dateCure;
        this.timeCure = builder.timeCure;
        this.isCalculate = builder.isCalculate;
    }

    public static class HistoryItemBuilder implements CommonModelBuilder<HistoryItem> {
        private final Long historyId;
        private final Long customerId;
        private final String customerName;
        private final String customerPhone;
        private final String registrationNumber;
        private final String medicalItemName;
        private final Double price;
        private final String isSalaryName;
        private final LocalDate dateCure;
        private final LocalTime timeCure;
        private final String isCalculate;

        public HistoryItemBuilder(ClinicHistory clinicHistory) {
            this.historyId = clinicHistory.getId();
            this.customerId = clinicHistory.getHospitalCustomer().getId();
            this.customerName = clinicHistory.getHospitalCustomer().getCustomerName();
            this.customerPhone = clinicHistory.getHospitalCustomer().getCustomerPhone();
            this.registrationNumber = clinicHistory.getHospitalCustomer().getRegistrationNumber();
            this.medicalItemName = clinicHistory.getMedicalItem().getName();
            this.price = clinicHistory.getPrice();
            this.isSalaryName = clinicHistory.getIsSalary() ? "예" : "아니오";
            this.dateCure = clinicHistory.getDateCure();
            this.timeCure = clinicHistory.getTimeCure();
            this.isCalculate = clinicHistory.getIsCalculate() ? "예" : "아니오";

        }

        @Override
        public HistoryItem build() {
            return new HistoryItem(this);
        }
    }
}
