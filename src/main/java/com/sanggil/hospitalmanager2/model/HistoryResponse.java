package com.sanggil.hospitalmanager2.model;

import com.sanggil.hospitalmanager2.entity.ClinicHistory;
import com.sanggil.hospitalmanager2.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class HistoryResponse {
    private Long customerId;
    private String customerName;
    private String customerPhone;
    private String registrationNumber;
    private String address;
    private String memo;
    private LocalDate dateCreate;
    private Long historyId;
    private String medicalItemName;
    private Double originPrice;
    private Double customerPrice;
    private String isSalary;
    private LocalDate dateCure;
    private LocalTime timeCure;
    private String isCalculate;

    private HistoryResponse(HistoryResponseBuilder builder) {
        this.customerId = builder.customerId;
        this.customerName = builder.customerName;
        this.customerPhone = builder.customerPhone;
        this.registrationNumber = builder.registrationNumber;
        this.address = builder.address;
        this.memo = builder.memo;
        this.dateCreate = builder.dateCreate;
        this.historyId = builder.historyId;
        this.medicalItemName = builder.medicalItemName;
        this.originPrice = builder.originPrice;
        this.customerPrice = builder.customerPrice;
        this.isSalary = builder.isSalary;
        this.dateCure = builder.dateCure;
        this.timeCure = builder.timeCure;
        this.isCalculate = builder.isCalculate;
    }

    public static class HistoryResponseBuilder implements CommonModelBuilder<HistoryResponse> {
        private final Long customerId;
        private final String customerName;
        private final String customerPhone;
        private final String registrationNumber;
        private final String address;
        private final String memo;
        private final LocalDate dateCreate;
        private final Long historyId;
        private final String medicalItemName;
        private final Double originPrice;
        private final Double customerPrice;
        private final String isSalary;
        private final LocalDate dateCure;
        private final LocalTime timeCure;
        private final String isCalculate;

        public HistoryResponseBuilder(ClinicHistory clinicHistory) {
            this.customerId = clinicHistory.getId();
            this.customerName = clinicHistory.getHospitalCustomer().getCustomerName();
            this.customerPhone = clinicHistory.getHospitalCustomer().getCustomerPhone();
            this.registrationNumber = clinicHistory.getHospitalCustomer().getRegistrationNumber();
            this.address = clinicHistory.getHospitalCustomer().getAddress();
            this.memo = clinicHistory.getHospitalCustomer().getMemo();
            this.dateCreate = clinicHistory.getHospitalCustomer().getDateCreate();
            this.historyId = clinicHistory.getId();
            this.medicalItemName = clinicHistory.getMedicalItem().getName();
            this.originPrice = clinicHistory.getMedicalItem().getNonSalaryPrice();
            this.customerPrice = clinicHistory.getPrice();
            this.isSalary = clinicHistory.getIsSalary() ? "Y" : "N";
            this.dateCure = clinicHistory.getDateCure();
            this.timeCure = clinicHistory.getTimeCure();
            this.isCalculate = clinicHistory.getIsCalculate() ? "Y" : "N";
        }

        @Override
        public HistoryResponse build() {
            return new HistoryResponse(this);
        }
    }
}
