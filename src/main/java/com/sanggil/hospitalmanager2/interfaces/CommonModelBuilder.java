package com.sanggil.hospitalmanager2.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}