package com.sanggil.hospitalmanager2.repository;

import com.sanggil.hospitalmanager2.entity.HospitalCustomer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HospitalCustomerRepository extends JpaRepository<HospitalCustomer, Long> {
}
