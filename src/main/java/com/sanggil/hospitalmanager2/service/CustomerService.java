package com.sanggil.hospitalmanager2.service;

import com.sanggil.hospitalmanager2.entity.HospitalCustomer;
import com.sanggil.hospitalmanager2.model.CustomerRequest;
import com.sanggil.hospitalmanager2.repository.HospitalCustomerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor
public class CustomerService {
    private final HospitalCustomerRepository hospitalCustomerRepository;

    public void setCustomer(CustomerRequest request) {
        HospitalCustomer addData = new HospitalCustomer.HospitalCustomerBuilder(request).build();

        hospitalCustomerRepository.save(addData);
    }

    public HospitalCustomer getData(long id) {
        return hospitalCustomerRepository.findById(id).orElseThrow();
    }

}
