package com.sanggil.hospitalmanager2.service;

import com.sanggil.hospitalmanager2.entity.ClinicHistory;
import com.sanggil.hospitalmanager2.entity.HospitalCustomer;
import com.sanggil.hospitalmanager2.model.*;
import com.sanggil.hospitalmanager2.repository.ClinicHistoryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class HistoryService {
    private final ClinicHistoryRepository clinicHistoryRepository;

    public void setHistory(HospitalCustomer hospitalCustomer, HistoryRequest historyRequest) {
        ClinicHistory addData = new ClinicHistory.ClinicHistoryBuilder(hospitalCustomer, historyRequest).build();

        clinicHistoryRepository.save(addData);
    }

    public List<HistoryItem> getHistoriesByDate(LocalDate searchDate) {
        List<ClinicHistory> originList = clinicHistoryRepository.findAllByDateCureOrderByIdDesc(searchDate);

        List<HistoryItem> result = new LinkedList<>();

        for (ClinicHistory item : originList) {
            HistoryItem addItem = new HistoryItem.HistoryItemBuilder(item).build();


            result.add(addItem);
        }

        return result;
    }

    public List<HistoryInsuranceCorporationItem> getHistoriesByInsurance(LocalDate searchDate) {
        List<ClinicHistory> originList = clinicHistoryRepository.findAllByIsSalaryAndDateCureAndIsCalculateOrderByIdDesc(true, searchDate, true);

            List<HistoryInsuranceCorporationItem> result = new LinkedList<>();

            for (ClinicHistory item : originList) {
                HistoryInsuranceCorporationItem addItem = new HistoryInsuranceCorporationItem();
                addItem.setCustomerName(item.getHospitalCustomer().getCustomerName());
                addItem.setCustomerPhone(item.getHospitalCustomer().getCustomerPhone());
                addItem.setRegistrationNumber(item.getHospitalCustomer().getRegistrationNumber());
                addItem.setMedicalItemName(item.getMedicalItem().getName());
                addItem.setMedicalItemNonSalaryPrice(item.getMedicalItem().getNonSalaryPrice());
                addItem.setCustomerContributionPrice(item.getPrice());
                addItem.setBillingAmount(item.getMedicalItem().getNonSalaryPrice() - item.getPrice());
                addItem.setDateCure(item.getDateCure());

                result.add(addItem);
            }

            return result;
    }

    public HistoryResponse getHistory(long id) {
        ClinicHistory originData = clinicHistoryRepository.findById(id).orElseThrow();

        return new HistoryResponse.HistoryResponseBuilder(originData).build();

    }

    public void putHistoryByPayComplete(long id) {
        ClinicHistory originData = clinicHistoryRepository.findById(id).orElseThrow();
        originData.putCompletePay();

        clinicHistoryRepository.save(originData);
    }

    public void putHistoryByClinicItem(long id, HistoryMedicalItemUpdateRequest updateRequest) {
        ClinicHistory originData = clinicHistoryRepository.findById(id).orElseThrow();
        originData.putMedicalItem(updateRequest);

        clinicHistoryRepository.save(originData);
    }

}
